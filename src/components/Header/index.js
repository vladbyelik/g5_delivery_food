import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { logOut } from '../../redux-store/actions';
import ModalAuth from '../ModalAuth';
import ModalCart from '../ModalCart';

const Header = ({ login, dispatch }) => {

  const [isLoginModal, setIsLoginModal] = useState(false);
  const [isBasketModal, setIsBasketModal] = useState(false);

  const handleLoginModal = () => {
    setIsLoginModal(!isLoginModal);
  };

  const handleBasketModal = () => {
    setIsBasketModal(!isBasketModal);
  };

  const handleLogOut = () => {
    dispatch(logOut());
  };

  const logo = require('./../../assets/img/icon/logo.svg').default;

  return (
    <header className='header'>
      <Link to="/">
        <img src={logo} alt="logo"/>
      </Link>

      <label className="address">
        <input type="text" className='input input-address' placeholder='адрес доставки'/>
      </label>
      
      <div className='buttons'>
        {login 
          ? 
            <>
              <span className='user-name'>{login}</span>
              <button onClick={handleBasketModal} className='button button-primary button auth'>
                <span className='button-cart-svg'></span>
                <span className='button-text'>Корзина</span>
              </button>

              <button className='button button-primary button auth' onClick={handleLogOut}>
                <span className='button-out-svg'></span>
                <span className='button-text'>Выйти</span>
              </button>
            </>
          :  
            <button onClick={handleLoginModal} className='button button-primary button auth'>
              <span className='button-auth-svg'></span>
              <span className='button-text'>Войти</span>
            </button>
        }
      </div>

      {isLoginModal && <ModalAuth onClose={handleLoginModal} />}
      {isBasketModal && <ModalCart onClose={handleBasketModal} login={login} />}

    </header>
  )
}

const mapStateToProps = (state) => ({
  login: state.login
})

export default connect(mapStateToProps)(Header);