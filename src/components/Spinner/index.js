import React from 'react';

const Spinner = ({ classPrefix }) => {

  const src = require(`../../assets/spinner.gif`);

  return (
    <img className={classPrefix} src={src} alt="spinner"/>
  )
}

export default Spinner;