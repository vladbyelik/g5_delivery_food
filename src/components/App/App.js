import usePageData from '../../custom-hooks/usePageData';
import Footer from '../Footer';
import Header from '../Header';
import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import HomePage from '../HomePage';
import './App.css';
import './style.css';
import ProductList from '../ProductList';

function App() {

  return (
    <div className="App">
      <main className='main'>
        <div className='container'>

          <Router>
            <Header />

              <Routes>
                
                <Route path="/" element={<HomePage />}/>
                <Route path="/restaurant/:products" element={<ProductList />} />

                <Route
                  path="*"
                  element={<div>Error</div>}
                />

              </Routes>
            <Footer />
          </Router>

        </div>
      </main>
    </div>
  );
}

export default App;
