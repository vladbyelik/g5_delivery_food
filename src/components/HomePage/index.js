import React from 'react';
import Banner from '../Banner';
import RestaurantList from '../RestaurantList';

const HomePage = () => {

  return (
    <>
      <Banner />
      <section className='restaurants'>
        
        <RestaurantList />
      </section>
    </>
  )
}

export default HomePage;