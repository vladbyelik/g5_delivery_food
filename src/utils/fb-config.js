import firebase from "firebase/app";
import 'firebase/auth';
import 'firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyB38yEnTS3DWoiHGx0n4AS9oWPtVCjTVCE",
  authDomain: "g5-df-e1772.firebaseapp.com",
  databaseURL: "https://g5-df-e1772-default-rtdb.firebaseio.com",
  projectId: "g5-df-e1772",
  storageBucket: "g5-df-e1772.appspot.com",
  messagingSenderId: "561187195361",
  appId: "1:561187195361:web:e58a51ccce693dbbc12aa0"
};

firebase.initializeApp(firebaseConfig);

export default firebase;

